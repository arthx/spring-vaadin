package su.arth;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import su.arth.service.UserService;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@Widgetset("su.arth.MyAppWidgetset")
@SpringUI
public class MyUI extends UI {

    @Autowired
    private UserService userService;

    @Autowired
    private SpringViewProvider springViewProvider;

    private Navigator navigator;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        System.out.println("init");
        final VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        root.setMargin(true);
        root.setSpacing(true);
        setContent(root);

        final Panel viewContainer = new Panel();
        viewContainer.setSizeFull();
        root.addComponent(viewContainer);
        root.setExpandRatio(viewContainer, 1.0f);


        navigator = new Navigator(this, viewContainer);
        navigator.addProvider(springViewProvider);
        setNavigator(navigator);
        navigator.navigateTo("login");

        //setContent(root);
    }


}
