package su.arth.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by arthur on 27.06.2016.
 */
@SpringView
public class LoginView extends VerticalLayout implements View {




    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        System.out.println("enter view login");
        setSizeFull();
        removeAllComponents();

        String params = viewChangeEvent.getParameters();
        addComponent(new Label(params));
    }
}
