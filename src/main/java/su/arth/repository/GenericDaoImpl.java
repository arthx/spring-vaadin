package su.arth.repository;

import org.springframework.stereotype.Repository;
import su.arth.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by arthur on 20.06.2016.
 */
@Repository
public class GenericDaoImpl<Object> implements GenericDao<Object> {

    @PersistenceContext
    EntityManager em;


    @Override
    public void create(Object entity) {
        em.persist(entity);

    }

    @Override
    public Object find(Object entity) {
        Class<?> aClass = null;
        Object result = null;
        try {
            aClass = entity.getClass();
                  switch (entity.getClass().getSimpleName()) {
                case "User":
                    result = (Object) em.find(aClass, ((User) entity).getIdUser());
                    break;
                case "Book":
                    result = (Object) em.find(aClass, ((Book) entity).getIdBook());
                    break;
                case "Role":
                    result = (Object) em.find(aClass, ((Role) entity).getIdRole());
                    break;
                case "Region":
                    result = (Object) em.find(aClass,((Region) entity).getIdRegion());
                   break;
                case "Street":
                    result = (Object) em.find(aClass,((Street) entity).getIdStreet());
                    break;
                case "City":
                    result = (Object) em.find(aClass,((City) entity).getIdCity());
                    break;
                case "Type":
                    result = (Object) em.find(aClass,((Type) entity).getIdType());
                    break;
                case "House":
                    result = (Object) em.find(aClass,((House) entity).getIdHouse());
                    break;
                default:
                    result = entity;
            }

        } catch (Exception e){
             e.printStackTrace();
        }
        return result;
    }

    @Override
    public void update(Object entity) {
          em.merge(entity);
    }

    @Override
    public void delete(Object entity) {
        em.remove(entity);
    }


    @Override
    public List<Object> findAll(Class<?> clazz) {
        List resultList = em.createQuery("from " + clazz.getSimpleName()).getResultList();
        return resultList;
    }
}
