package su.arth.repository;

import org.springframework.stereotype.Repository;
import su.arth.entity.File;
import su.arth.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by arthur on 20.06.2016.
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {


    @PersistenceContext
    EntityManager em;

    @Override
    public User findByLogin(String login) {
        return null;
    }

    @Override
    public User findByEmail(String email) {
        return null;
    }



    public File getDefaultProfilePhoto(){
         return em.getReference(File.class,1L);
    }


}
