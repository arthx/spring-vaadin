package su.arth.repository;


import org.springframework.stereotype.Repository;
import su.arth.entity.Role;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by arthur on 21.06.2016.
 */
@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role> implements RoleDao {

    @PersistenceContext
    EntityManager em;

    @Override
    public void deleteRole() {

    }



    @Override
    public Role getDefaultRole() {

        return em.getReference(Role.class,1L);
    }
}
