package su.arth.repository;

import org.springframework.stereotype.Repository;
import su.arth.entity.Book;
import su.arth.entity.File;

/**
 * Created by arthur on 20.06.2016.
 */
@Repository
public interface BookDao extends GenericDao<Book> {


    Book findByName(String name);

   public File getDefaultBookImage();

}
