package su.arth.repository;

import org.springframework.stereotype.Repository;
import su.arth.entity.Book;
import su.arth.entity.File;

/**
 * Created by arthur on 20.06.2016.
 */
@Repository
public class BookDaoImpl extends GenericDaoImpl<Book> implements BookDao {

    @Override
    public Book findByName(String name) {
        return null;
    }


    @Override
    public File getDefaultBookImage() {

        return em.find(File.class,1L);
    }
}
