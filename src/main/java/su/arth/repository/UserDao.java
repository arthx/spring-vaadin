package su.arth.repository;

import org.springframework.stereotype.Repository;
import su.arth.entity.File;
import su.arth.entity.User;

/**
 * Created by arthur on 20.06.2016.
 */
@Repository
public interface UserDao extends GenericDao<User> {


    User findByLogin(String login);
    User findByEmail(String email);
     File getDefaultProfilePhoto();

}
