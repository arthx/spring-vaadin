package su.arth.repository;

import org.springframework.stereotype.Repository;
import su.arth.entity.Role;

/**
 * Created by arthur on 21.06.2016.
 */
@Repository
public interface RoleDao extends GenericDao<Role>{


    Role getDefaultRole();



    void deleteRole();





}
