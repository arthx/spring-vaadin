package su.arth.repository.address;

import org.springframework.stereotype.Repository;
import su.arth.entity.Street;
import su.arth.repository.GenericDaoImpl;

/**
 * Created by arthur on 22.06.2016.
 */
@Repository
public class StreetDaoImpl extends GenericDaoImpl<Street> implements StreetDao {


}
