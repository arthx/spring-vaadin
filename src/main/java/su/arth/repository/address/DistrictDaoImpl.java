package su.arth.repository.address;

import org.springframework.stereotype.Repository;
import su.arth.entity.District;
import su.arth.repository.GenericDaoImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by arthur on 22.06.2016.
 */
@Repository
public class DistrictDaoImpl extends GenericDaoImpl<District> implements DistrictDao {


    @PersistenceContext
    EntityManager em;

}
