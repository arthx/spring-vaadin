package su.arth.repository.address;

import org.springframework.stereotype.Repository;
import su.arth.entity.Location;
import su.arth.repository.GenericDaoImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by arthur on 22.06.2016.
 */
@Repository
public class LocationDaoImpl extends GenericDaoImpl<Location> implements LocationDao {

    @PersistenceContext
    EntityManager em;


    @Override
    public void addLocation(Location location) {


    }
}
