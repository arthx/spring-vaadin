package su.arth.repository;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by arthur on 20.06.2016.
 */
@Repository
public interface GenericDao<T> {

    /** Сохранить объект newInstance в базе данных */
    void create(T entity);

    /** Извлечь объект, предварительно сохраненный в базе данных, используя
     *   указанный id в качестве первичного ключа
     */
    T find(T entity);

    /** Сохранить изменения, сделанные в объекте.  */
    void update(T entity);

    /** Удалить объект из базы данных */
    void delete(T entity);

List<T> findAll(Class<?> clazz);

}
