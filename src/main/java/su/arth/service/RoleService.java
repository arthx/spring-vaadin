package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.entity.Role;
import su.arth.repository.RoleDao;

/**
 * Created by arthur on 21.06.2016.
 */
@Service
public class RoleService {

  @Autowired
    private RoleDao roleDao;



    public Role getDefaultRole(){
        return roleDao.getDefaultRole();
    }

    @Transactional
public void create(Role role){
    roleDao.create(role);
}

}
