package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.entity.*;
import su.arth.repository.RoleDao;
import su.arth.repository.UserDao;

/**
 * Created by arthur on 20.06.2016.
 */
@Service
@Transactional
public class UserService {


    @Autowired
    private UserDao userDao;

   @Autowired
    private RoleDao roleDao;

    @Autowired
    PasswordEncoder passwordEncoder;



    public void addUser(User user){
        userDao.create(user);
    }

    public User findUser(User user){
        return userDao.find(user);
    }


    public void updateUser(User user){
        userDao.update(user);
    }

       private Settings getDefaultSettings(){

    return null;
}


   public void createUser(User user){
       user.getRoles()
               .add(roleDao.getDefaultRole());
       user.setPassword(passwordEncoder.encode(user.getPassword()));
       user.setProfile_photo(userDao.getDefaultProfilePhoto());
       user.setSettings(getDefaultSettings());
       Location location = user.getLocation();
       Street street = location.getStreet();
       City city = location.getCity();
       District district = location.getDistrict();
       House house = location.getHouse();
       Region region = location.getRegion();


      /* tx.begin();
       em.persist(city);
       if(district!=null) em.persist(district);
       if(region!=null) em.persist(region);
       if(house!=null) em.persist(house);
       em.persist(street);
       em.persist(location);*/


       userDao.create(user);

//       tx.commit();
   }







}
