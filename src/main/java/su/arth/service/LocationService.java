package su.arth.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.entity.*;
import su.arth.repository.address.*;
import su.arth.service.kladr.KladrLocation;

import java.util.List;

/**
 * Created by arthur on 22.06.2016.
 */
@Service
public class LocationService {

    @Autowired
    LocationDao locationDao;

    @Autowired
    public DistrictDao districtDao;

    @Autowired
    public RegionDao regionDao;
    @Autowired
    public StreetDao streetDao;
    @Autowired
    public HouseDao houseDao;

@Autowired
public CityDao cityDao;


    @Autowired
    private KladrLocation kladrLocation;


    @Transactional
    public void addLocation(Location location){

        District district;

        City city;
        House house;
        Region region;
        Street street;


        if((house = location.getHouse())!=null){
            House h;
           if((h = houseDao.find(house))==null){
               houseDao.create(house);
           } else {
               house = h;
           }
        }
        if((district = location.getDistrict())!=null){
            District d;
            if((d = districtDao.find(district))==null){

                districtDao.create(district);
            } else {
                district = d;
            }
        }

        if(( region = location.getRegion())!=null){
            Region r;
            if((r = regionDao.find(region))==null){
                regionDao.create(region);}
            else {
                region = r;
            }
        }
        if((street = location.getStreet())!=null){
            Street s;
            if((s = streetDao.find(street))==null) {
                streetDao.create(street);
            }
            else {
               street = s;
            }
        }

        if((city = location.getCity())!=null){
            City c;
            if((c = cityDao.find(city))==null){

                cityDao.create(city);
            } else {
                city = c;
            }
        }

        location.setCity(city);
        location.setRegion(region);
        location.setStreet(street);
        houseDao.create(house);
        locationDao.create(location);
    }

    public void locTest(){
        List city = kladrLocation.getCity("Челя", true);
    }




}
