package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.entity.Book;
import su.arth.entity.User;
import su.arth.repository.BookDao;
import su.arth.repository.UserDao;

import java.util.List;
import java.util.Set;

/**
 * Created by arthur on 21.06.2016.
 */
@Service
public class BookService {

    @Autowired
    private BookDao bookDao;


    @Autowired
    private UserDao userDao;
@Transactional
public void addMyBook(Book book){
    User user = new User();
    user.setIdUser(3L);
    user = userDao.find(user);
//    VaadinSession.getCurrent().setAttribute(User.class.getName(), user);
  //  user = (User) VaadinSession.getCurrent().getAttribute(User.class.getName());
    List<Book> mybooks = user.getMybooks();
    book.setOwner(user);
    book.setImage(bookDao.getDefaultBookImage());
    bookDao.create(book);
    mybooks.add(book);
    user.setMybooks(mybooks);


}
    @Transactional
    public void addWishBook(Book book){
        User user = new User();
        user.setIdUser(3L);
        user = userDao.find(user);

        List<Book> wishbooks = user.getWishbooks();
         Book b;
        if((b = bookDao.find(book))==null){
            bookDao.create(book);

        } else {
            book = b;
        }

        wishbooks.add(book);
        user.setWishbooks(wishbooks);
        userDao.update(user);
    }


    public Book findBook(Book book){
      return bookDao.find(book);

    }

    @Transactional
    public void deleteBookFromListMyBooks(Book book){
        User user = new User();
        user.setIdUser(3L);
        user = userDao.find(user);
        book = bookDao.find(book);
        //User owner = book.getOwner();
       // book.setOwner(null);
        List<Book> mybooks = user.getMybooks();
        mybooks.remove(book);

        user.setMybooks(mybooks);
         bookDao.delete(book);
    }

    @Transactional
    public void deleteBookFromListWishBooks(Book book){
        User user = new User();
        user.setIdUser(3L);
        user = userDao.find(user);
        book = bookDao.find(book);

        Set<User> users = book.getUser();
        users.remove(user);
        book.setUser(users);
        List<Book> wishbooks = user.getWishbooks();
        wishbooks.remove(book);
        user.setWishbooks(wishbooks);

    }

}
