package su.arth.service.kladr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by salimhanov on 04.04.2016.
 */
@Service
public class KladrLocation {

 private final static String KEY = "57023d3d0a69de880f8b456f";

    @Autowired
    private KladrApiClient client;




    public List getCity(String str,boolean par){
        List<KladrObject> cityNameList = null;
        try {
     cityNameList = client.getKladrCities(str, 20, par);
} catch (Exception e){

        }
        return cityNameList;

    }


    public boolean isStreetsInCity(String city){
        if(getStreet(city,"").size()>0){
            return true;
        }
        return false;
            }


    public List getStreet(String city,String str){


        List<KladrObject> streetList = client.getKladrStreetsByCityName(str, city, 5);
      return streetList;
    }


}
