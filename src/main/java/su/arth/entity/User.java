package su.arth.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by arthur on 01.04.16.
 */
@Entity
@Table(name = "user")
public class User implements Serializable {
    private long idUser;
    private String login;
    private String password;
    private String lastname;
    private String firstname;
    private String email;
    private String phone;
    private Location location;
    private Byte sex;
    private Double scope;

    private Set<Role> roles = new HashSet<>();
    private Set<Tag> tags = new HashSet<>();
    private Settings settings;
    private File profile_photo;
    private List<Book> mybooks = new ArrayList<>();
    private List<Book> wishbooks= new ArrayList<>();


    public User(String login, String password, String lastname, String firstname, String email, String phone, Location location, Byte sex) {
        this.login = login;
        this.password = password;
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.phone = phone;
        this.location = location;
        this.sex = sex;
    }

    public User() {
    }

    @Id
    @Column(name = "id_user")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "lastname")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "firstname")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="location_id",referencedColumnName = "id_location")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Basic
    @Column(name = "sex")
    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "scope")
    public Double getScope() {
        return scope;
    }

    public void setScope(Double scope) {
        this.scope = scope;
    }


    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_role",
            joinColumns = { @JoinColumn(name = "user_id",referencedColumnName = "id_user") },
            inverseJoinColumns = { @JoinColumn(name = "role_id",referencedColumnName = "id_role") })
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }



    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(name = "user_tag",
            joinColumns = { @JoinColumn(name = "user_id",referencedColumnName = "id_user") },
            inverseJoinColumns = { @JoinColumn(name = "tag_id",referencedColumnName = "id_tag") })
    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    @OneToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "owner")
        public List<Book> getMybooks() {
        return mybooks;
    }

    public void setMybooks(List<Book> mybooks) {
        this.mybooks = mybooks;
    }






    @ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE })
    @JoinTable(name = "wishbook",
            joinColumns = { @JoinColumn(name = "user_id",referencedColumnName = "id_user") },
            inverseJoinColumns = { @JoinColumn(name = "book_id",referencedColumnName = "id_book") })
    public List<Book> getWishbooks() {
        return wishbooks;
    }

    public void setWishbooks(List<Book> wishbooks) {
        this.wishbooks = wishbooks;
    }




    @OneToOne
    @JoinColumn(name="settings",referencedColumnName = "id_settings")
    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    @OneToOne
    @JoinColumn(name="profile_photo",referencedColumnName = "id_file")
    public File getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(File profile_photo) {
        this.profile_photo = profile_photo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (idUser != user.idUser) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (lastname != null ? !lastname.equals(user.lastname) : user.lastname != null) return false;
        if (firstname != null ? !firstname.equals(user.firstname) : user.firstname != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (phone != null ? !phone.equals(user.phone) : user.phone != null) return false;
        if (location != null ? !location.equals(user.location) : user.location != null) return false;
        if (sex != null ? !sex.equals(user.sex) : user.sex != null) return false;
        if (scope != null ? !scope.equals(user.scope) : user.scope != null) return false;
        if (roles != null ? !roles.equals(user.roles) : user.roles != null) return false;
        if (tags != null ? !tags.equals(user.tags) : user.tags != null) return false;
        if (settings != null ? !settings.equals(user.settings) : user.settings != null) return false;
        if (profile_photo != null ? !profile_photo.equals(user.profile_photo) : user.profile_photo != null)
            return false;
        return mybooks != null ? mybooks.equals(user.mybooks) : user.mybooks == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idUser ^ (idUser >>> 32));
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (settings != null ? settings.hashCode() : 0);
        result = 31 * result + (profile_photo != null ? profile_photo.hashCode() : 0);
        result = 31 * result + (mybooks != null ? mybooks.hashCode() : 0);
        return result;
    }
}
