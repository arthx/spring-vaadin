package su.arth.entity;

import javax.persistence.*;

/**
 * Created by arthur on 10.04.16.
 */
@Entity
@Table(name = "city")
public class City {
    private String idCity;
    private String name;
    private String zip;
    private String okato;
  private Type type;


    @Id
    @Column(name = "id_city")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "zip")
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "okato")
    public String getOkato() {
        return okato;
    }

    public void setOkato(String okato) {
        this.okato = okato;
    }

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="type_id",referencedColumnName = "id_type")
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        if (idCity != null ? !idCity.equals(city.idCity) : city.idCity != null) return false;
        if (name != null ? !name.equals(city.name) : city.name != null) return false;
        if (zip != null ? !zip.equals(city.zip) : city.zip != null) return false;
        if (okato != null ? !okato.equals(city.okato) : city.okato != null) return false;
        return type != null ? type.equals(city.type) : city.type == null;

    }

    @Override
    public int hashCode() {
        int result = idCity != null ? idCity.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (okato != null ? okato.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
