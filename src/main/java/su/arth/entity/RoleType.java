package su.arth.entity;

/**
 * Created by arthur on 20.01.16.
 */
public enum RoleType {


    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN");


    String roleType;

    private RoleType(String roleType){
        this.roleType = roleType;
    }

    public String getRoleType(){
        return roleType;
    }
}
