package su.arth.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by arthur on 07.04.16.
 */
@Entity
@Table(name = "house")
public class House implements Serializable {
    private long idHouse;
    private String number;

    @Id
    @Column(name = "id_house")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getIdHouse() {
        return idHouse;
    }

    public void setIdHouse(long idHouse) {
        this.idHouse = idHouse;
    }

    @Basic
    @Column(name = "number")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        House house = (House) o;

        if (idHouse != house.idHouse) return false;
        if (number != null ? !number.equals(house.number) : house.number != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idHouse ^ (idHouse >>> 32));
        result = 31 * result + (number != null ? number.hashCode() : 0);
        return result;
    }
}
