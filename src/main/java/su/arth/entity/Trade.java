package su.arth.entity;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by arthur on 01.04.16.
 */
@Entity
@Table(name = "trade")
public class Trade {
    private long idTrade;
    private Date tradeDate;
    private Date planedDate;
    private Date reverseDate;
    private Long location;
    private String status;
    private Double scope;
    private String comment;
    private Long book;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_trade")
    public long getIdTrade() {
        return idTrade;
    }

    public void setIdTrade(long idTrade) {
        this.idTrade = idTrade;
    }

    @Basic
    @Column(name = "trade_date")
    public Date getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(Date tradeDate) {
        this.tradeDate = tradeDate;
    }

    @Basic
    @Column(name = "planed_date")
    public Date getPlanedDate() {
        return planedDate;
    }

    public void setPlanedDate(Date planedDate) {
        this.planedDate = planedDate;
    }

    @Basic
    @Column(name = "reverse_date")
    public Date getReverseDate() {
        return reverseDate;
    }

    public void setReverseDate(Date reverseDate) {
        this.reverseDate = reverseDate;
    }

    @Basic
    @Column(name = "location")
    public Long getLocation() {
        return location;
    }

    public void setLocation(Long location) {
        this.location = location;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "scope")
    public Double getScope() {
        return scope;
    }

    public void setScope(Double scope) {
        this.scope = scope;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "book_id")
    public Long getBook() {
        return book;
    }

    public void setBook(Long book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trade trade = (Trade) o;

        if (idTrade != trade.idTrade) return false;
        if (tradeDate != null ? !tradeDate.equals(trade.tradeDate) : trade.tradeDate != null) return false;
        if (planedDate != null ? !planedDate.equals(trade.planedDate) : trade.planedDate != null) return false;
        if (reverseDate != null ? !reverseDate.equals(trade.reverseDate) : trade.reverseDate != null) return false;
        if (location != null ? !location.equals(trade.location) : trade.location != null) return false;
        if (status != null ? !status.equals(trade.status) : trade.status != null) return false;
        if (scope != null ? !scope.equals(trade.scope) : trade.scope != null) return false;
        if (comment != null ? !comment.equals(trade.comment) : trade.comment != null) return false;
        if (book != null ? !book.equals(trade.book) : trade.book != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idTrade ^ (idTrade >>> 32));
        result = 31 * result + (tradeDate != null ? tradeDate.hashCode() : 0);
        result = 31 * result + (planedDate != null ? planedDate.hashCode() : 0);
        result = 31 * result + (reverseDate != null ? reverseDate.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (book != null ? book.hashCode() : 0);
        return result;
    }
}
