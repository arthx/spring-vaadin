package su.arth.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by arthur on 01.04.16.
 */
@Entity
@Table(name = "book")
public class Book {
    private long idBook;
    private String name;
    private String author;
    private String publishing;
    private Date releaseDate;
    private String version;
    private Double score;
    private Set<Tag> tags;
    private Set<User> user = new HashSet<>();
    private User owner;
    private File image;
//    private int bookType;


    public Book() {
    }

    public Book(String name, String author, String publishing, Date releaseDate, String version) {
        this.name = name;
        this.author = author;
        this.publishing = publishing;
        this.releaseDate = releaseDate;
        this.version = version;
    }

    public Book(String name, String author, String version) {
        this.name = name;
        this.author = author;
        this.version = version;
    }

    @Id
    @Column(name = "id_book")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getIdBook() {
        return idBook;
    }

    public void setIdBook(long idBook) {
        this.idBook = idBook;
    }

/*@JoinColumn(table = "user_book", name = "book_type")
  *//*  @JoinTable(name = "user_book",
            joinColumns = @JoinColumn(name = "book_type"))
  *//*  public int getBookType() {
        return bookType;
    }

    public void setBookType(int bookType) {
        this.bookType = bookType;
    }*/









    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "publishing")
    public String getPublishing() {
        return publishing;
    }

    public void setPublishing(String publishing) {
        this.publishing = publishing;
    }


    @Basic
    @Column(name = "release_date")
    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Basic
    @Column(name = "version")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "score")
    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }


    @ManyToMany(fetch= FetchType.LAZY,cascade = {CascadeType.PERSIST, CascadeType.MERGE})
        @JoinTable(name = "wishbook",
                   joinColumns = { @JoinColumn(name="book_id",referencedColumnName = "id_book" )},
                   inverseJoinColumns = {@JoinColumn(name = "user_id",referencedColumnName = "id_user")})
    //@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL,mappedBy = "mybooks")
    public Set<User> getUser() {
        return user;
    }

    public void setUser(Set<User> user) {
        this.user = user;
    }

    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    @JoinColumn(name = "owner")
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }




    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(name = "tag_book",
            joinColumns = { @JoinColumn(name = "book_id",referencedColumnName = "id_book") },
            inverseJoinColumns = { @JoinColumn(name = "tag_id",referencedColumnName = "id_tag") })
    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    @OneToOne
    @JoinColumn(name="image",referencedColumnName = "id_file")
    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (idBook != book.idBook) return false;
        if (name != null ? !name.equals(book.name) : book.name != null) return false;
        if (author != null ? !author.equals(book.author) : book.author != null) return false;
        if (publishing != null ? !publishing.equals(book.publishing) : book.publishing != null) return false;
        if (releaseDate != null ? !releaseDate.equals(book.releaseDate) : book.releaseDate != null) return false;
        if (version != null ? !version.equals(book.version) : book.version != null) return false;
        if (score != null ? !score.equals(book.score) : book.score != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idBook ^ (idBook >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (publishing != null ? publishing.hashCode() : 0);
        result = 31 * result + (releaseDate != null ? releaseDate.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (score != null ? score.hashCode() : 0);
        return result;
    }
}
