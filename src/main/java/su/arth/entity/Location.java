package su.arth.entity;

import javax.persistence.*;

/**
 * Created by arthur on 07.04.16.
 */
@Entity
@Table(name = "location")
public class Location {
    private long idLocation;
    private Region region;
    private District district;
    private Street street;
    private House house;
    private City city;


    @Id
    @Column(name = "id_location")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(long idLocation) {
        this.idLocation = idLocation;
    }

    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="region_id",referencedColumnName = "id_region")
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="district_id",referencedColumnName = "id_district")
    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="street_id",referencedColumnName = "id_street")
    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="house_id",referencedColumnName = "id_house")
    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="city_id",referencedColumnName = "id_city")
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (idLocation != location.idLocation) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (idLocation ^ (idLocation >>> 32));
    }
}
