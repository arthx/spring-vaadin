package su.arth.entity;

import javax.persistence.*;

/**
 * Created by arthur on 07.04.16.
 */
@Entity
@Table(name = "region")
public class Region {
    private String idRegion;
    private String name;
    private String zip;
    private String okato;
    private Type type;

    @Id
    @Column(name = "id_region")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "zip")
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "okato")
    public String getOkato() {
        return okato;
    }

    public void setOkato(String okato) {
        this.okato = okato;
    }


    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="type_id",referencedColumnName = "id_type")
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Region region = (Region) o;

        if (idRegion != null ? !idRegion.equals(region.idRegion) : region.idRegion != null) return false;
        if (name != null ? !name.equals(region.name) : region.name != null) return false;
        if (zip != null ? !zip.equals(region.zip) : region.zip != null) return false;
        if (okato != null ? !okato.equals(region.okato) : region.okato != null) return false;
        return type != null ? type.equals(region.type) : region.type == null;

    }

    @Override
    public int hashCode() {
        int result = idRegion != null ? idRegion.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (okato != null ? okato.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
