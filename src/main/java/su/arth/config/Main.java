package su.arth.config;

import java.util.stream.IntStream;

/**
 * Created by arthur on 29.06.2016.
 */
public class Main {

    public static void main(String[] args) {


    final long begin = System.currentTimeMillis();

    int[] ints = IntStream.range(0, 15)
            .parallel()
            .filter(Main::check)
            .toArray();

    System.out.println("total numbers: " + ints.length + ", time: " +
            (System.currentTimeMillis() - begin)/1000 + " s");
}

    private static boolean check(int n) {
        final boolean[] digits = new boolean[10];
        while (n > 0) {
            final int d = n % 10;
            if (digits[d]) {
                return false;
            } else {
                n = n / 10;
                digits[d] = true;
            }
        }
        return true;
    }




}
