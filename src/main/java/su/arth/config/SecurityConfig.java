package su.arth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by arthur on 21.06.2016.
 */
@Configuration
@ComponentScan({"su.arth"})
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
//        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }





    @Override
    protected void configure(HttpSecurity http) throws Exception {
     //  http.authorizeRequests().antMatchers("/","/log1n","/VAADIN/**", "/PUSH/**", "/UIDL/**").permitAll().and().csrf().disable();

        http.authorizeRequests().antMatchers("/VAADIN/**","/#!login","/login").permitAll().and().csrf().disable();



        http.authorizeRequests().antMatchers("/").access("isAuthenticated()").and();

//access("isAuthenticated()")
                                         http.formLogin()
                .loginPage("/login")
                .failureUrl("/error")
                .usernameParameter("login")
                .passwordParameter("password").permitAll()
                .and().logout().logoutSuccessUrl("/signin?logout")
                .and().csrf()
                .and().exceptionHandling().accessDeniedPage("/403").and();

/*

        http.authorizeRequests().antMatchers("/admin")
                .access("hasRole('ROLE_ADMIN')").and();
*/


       /* http.formLogin()
                .loginPage("/signin")
                .loginProcessingUrl("/j_spring_security_check")
                .failureUrl("/error")
                .usernameParameter("login")
                .passwordParameter("password")
                .permitAll().and();

        http.logout()

                .logoutUrl("/logout")
                .permitAll()
                //.logoutSuccessUrl("/")
                .invalidateHttpSession(true).and().csrf()
                .and().exceptionHandling().accessDeniedPage("/403").and();

        http.sessionManagement().maximumSessions(1);


*/

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }




    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }
   /* @Bean
    ExceptionMappingAuthenticationFailureHandler authenticationFailureHandler(){
        ExceptionMappingAuthenticationFailureHandler ex = new ExceptionMappingAuthenticationFailureHandler();
        Map<String, String> mappings = new HashMap<String, String>();
        mappings.put("org.springframework.security.authentication.CredentialsExpiredException", "/signin?error=reset");
        mappings.put("org.springframework.security.authentication.LockedException", "/signin?error=locked");
        mappings.put("org.springframework.security.authentication.BadCredentialsException", "/signin?error=creditials");
        mappings.put("org.springframework.security.core.userdetails.UsernameNotFoundException", "/signin?error=usernf");

        ex.setExceptionMappings(mappings);
        return ex;
    }*/




}
