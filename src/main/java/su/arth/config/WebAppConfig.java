package su.arth.config;

import com.vaadin.spring.annotation.EnableVaadin;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import su.arth.service.MyUserDetailsService;
import su.arth.service.kladr.KladrApiClient;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by arthur on 07.06.16.
 */
@Configuration
@ComponentScan("su.arth")
@EnableVaadin
@EnableTransactionManagement
@PropertySource("classpath:config/db.properties")
//@Import(SecurityConfig.class)
public class WebAppConfig implements EnvironmentAware {
    private Environment env;

    @Override
    public void setEnvironment(Environment environment) {
        this.env=environment;
    }

    @Bean
    public PropertySourcesPlaceholderConfigurer placeholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(name = "userDetailsService")
    public UserDetailsService myUserDetailsService(){
        return new MyUserDetailsService();
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
     //   em.setDataSource(dataSource());
        em.setPersistenceUnitName("bookdb");
        em.setPersistenceXmlLocation("classpath:/META-INF/persistence.xml");
        em.setPackagesToScan(new String[] {"su.arth.entity"});

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
       // em.setJpaProperties(additionalProperties());

        return em;
    }



    @Bean
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/bookdb");
        dataSource.setUsername( "root" );
        dataSource.setPassword( "11011" );
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
        return new PersistenceExceptionTranslationPostProcessor();
    }


    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return properties;
    }


@Bean
@Scope("singleton")
public KladrApiClient kladrApiClient(){
    return new KladrApiClient("57023d3d0a69de880f8b456f", "57023d3d0a69de880f8b456f");
}

/*
    @Bean
    public EntityManagerFactory entityManagerFactory() {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("ru.arth.entity");
//        factory.setDataSource(getDataSource());
        factory.setPersistenceUnitName("books");
        factory.afterPropertiesSet();

        return factory.getObject();
    }




    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }
*/


    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        //  org.apache.commons.dbcp2.BasicDataSource ds = new org.apache.commons.dbcp2.BasicDataSource();
        com.mchange.v2.c3p0.ComboPooledDataSource ds = new com.mchange.v2.c3p0.ComboPooledDataSource();
        try {
            String driverClassName = env.getProperty("jdbc.driverClassName");
            String url = env.getProperty("jdbc.url");
            String user = env.getProperty("jdbc.username");
            String password = env.getProperty("jdbc.password");
            int minPoolSize = Integer.parseInt(env.getProperty("jdbc.minPoolSize"));
            int maxPoolSize = Integer.parseInt(env.getProperty("jdbc.maxPoolSize"));
            int maxStatements = Integer.parseInt(env.getProperty("jdbc.maxStatements"));


            ds.setDriverClass(driverClassName);
            ds.setJdbcUrl(url);
            ds.setUser(user);
            ds.setPassword(password);
            ds.setMinPoolSize(minPoolSize);
            //          ds.setAcquireIncrement(2);
            ds.setMaxPoolSize(maxPoolSize);
            ds.setMaxStatements(maxStatements);

        } catch (Exception e) {
//            logger.error(e.getMessage());
        }
        return ds;
    }


  /*  @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
*/

}
