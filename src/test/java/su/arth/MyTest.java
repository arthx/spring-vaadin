package su.arth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import su.arth.config.WebAppConfig;
import su.arth.entity.Book;
import su.arth.repository.address.*;
import su.arth.service.BookService;
import su.arth.service.LocationService;
import su.arth.service.RoleService;
import su.arth.service.UserService;

/**
 * Created by arthur on 20.06.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppConfig.class})
@WebAppConfiguration
public class MyTest {



    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    LocationService locationService;
    @Autowired
    LocationDao locationDao;

    @Autowired
    public DistrictDao districtDao;

    @Autowired
    public RegionDao regionDao;
    @Autowired
    public StreetDao streetDao;
    @Autowired
    public HouseDao houseDao;
    @Autowired
    public CityDao cityDao;
    @Autowired
    TypeDao typeDao;

    @Autowired
    private BookService service;

@Test
    public void testMethod(){

}

/*@Test
public void test(){

    User user = new User();
    user.setIdUser(3L);

    User bdUser = userService.findUser(user);

    bdUser.setLastname("тест имя");

    userService.updateUser(bdUser);

}*/


/*    @Test
    public void testLocationService(){
        Location location = new Location();

        Region region = regionDao.findAll(Region.class).get(0);
       City city = cityDao.findAll(City.class).get(0);
        House house = new House();
        house.setNumber("22");
        Street street = streetDao.findAll(Street.class).get(0);
        location.setCity(city);
        location.setHouse(house);
        location.setRegion(region);
        location.setStreet(street);
        locationService.addLocation(location);


    }*/




 /*   @Test
    public void createRole(){
        Role role = new Role();
        role.setRolename("ROLE_USER");
        roleService.create(role);
    }*/


  /*  @Test
    public void locTest(){
        locationService.locTest();
    }*/

    @Test
   public void bookServiceTest(){
       Book book = new Book();
        book.setName("booktest");
        book.setAuthor("test");

        service.addMyBook(book);
    }
    @Test
    public void addWishBookTest(){
        Book book = new Book();
        book.setIdBook(4L);
        service.addWishBook(book);
    }


    @Test
    public void deleteBook(){
        Book book = new Book();
        book.setIdBook(4L);
        //service.deleteBookFromListWishBooks(book);
service.deleteBookFromListMyBooks(book);
    }

    @Test
    public void addMyBookTest(){
        Book book = new Book();
        book.setName("testetest");
        book.setAuthor("dfgfdgdfg");
        service.addMyBook(book);
    }

}
